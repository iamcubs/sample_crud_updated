<?php
	
	function database(){
		$username = 'root';
		$password = '';
		return NEW PDO('mysql:host=localhost;dbname=sample_crud', $username, $password);
	}

	// function dataList(){
	// 	$db = database();
	// 	$sql = "SELECT * FROM crud_data WHERE status = ?";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute(array(1));
	// 	$rows = $cmd->fetchAll(); //USE fetchAll(); if you want to retrieve a MULTIPLE ROW of data
	// 	$db = null;

	// 	return $rows;
	// }

	// function createData($name, $desc, $course){
	// 	$db = database();
	// 	$sql = "INSERT INTO crud_data(name, description, course, status) VALUES(?, ?, ?, ?)";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute(array($name, $desc, $course, 1));
	// 	$db = null;

	// 	return 'success';
	// }

	// function getUser($id){
	// 	$db = database();
	// 	$sql = "SELECT * FROM crud_data WHERE id = ?";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute(array($id));
	// 	$row = $cmd->fetch(); //USE fetch(); if you want to retrieve a SINGLE OR ONE ROW of data
	// 	$db = null;

	// 	return $row;
	// }

	// function updateData($edt_name, $edt_desc, $edt_course, $id){
	// 	$db = database();
	// 	$sql = "UPDATE crud_data SET name = ?, description = ?, course = ? WHERE id = ?";
	// 	$cmd =$db->prepare($sql);
	// 	$cmd->execute(array($edt_name, $edt_desc, $edt_course, $id));
	// 	$db = null;

	// 	return 'success';
	// }

	// function deleteData($id){
	// 	$db = database();
	// 	$sql = "UPDATE crud_data SET status = ? WHERE id = ?";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute(array(0, $id));
	// 	$db = null;

	// 	return 'success';
	// }

	// function insertFilename($filename){
	// 	$db = database();
	// 	$sql = "INSERT  INTO data_text(file_name, date_created) VALUES('$filename', NOW())";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute();
	// 	$db = null;

	// 	return 'success';
	// }

	// function fileList(){
	// 	$db = database();
	// 	$sql = "SELECT * FROM data_text";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute();
	// 	$rows = $cmd->fetchAll();
	// 	$db = null;

	// 	return $rows;
	// }

	// function getTextData($id){
	// 	$db = database();
	// 	$sql = "SELECT * FROM data_text WHERE id = ?";
	// 	$cmd = $db->prepare($sql);
	// 	$cmd->execute(array($id));
	// 	$data = $cmd->fetch();
	// 	$db = null;

	// 	return $data;
	// }

	//  CK EDITOR DB START 
	function insertCkData($title, $filename){
		$db = database();
		$sql = "INSERT INTO ck_editor(title, file_name, date_created) VALUES('$title', '$filename', NOW())";
		$cmd = $db->prepare($sql);
		$cmd->execute();
		$db = null;
		return 'success';
	}

	function displayckData(){
		$db = database();
		$sql = "SELECT * FROM ck_editor";
		$cmd = $db->prepare($sql);
		$cmd->execute();
		$rows = $cmd->fetchAll();
		$db = null;

		return $rows;
	}

	function getCKDATA($id){
		$db = database();
		$sql = "SELECT * FROM ck_editor WHERE id = ?";
		$cmd = $db->prepare($sql);
		$cmd->execute(array($id));
		$row = $cmd->fetch();
		$db = null;
		return $row;
	}


?>
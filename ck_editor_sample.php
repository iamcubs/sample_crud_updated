<!DOCTYPE html>
<html>
<head>
	<?php 
		require('header.php'); 
		// echo date('Ymdhisa');
	?>
</head>
<body style="padding-top: 5%;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<label>Title:</label>
				<input type="text" class="form-control" id="ck_title"><br>
				<textarea class="ckeditor" name="ckeditor" id="ckeditor"></textarea>
				<button class="btn btn-primary" onclick="ckEditordata()" id="btn_ck_save"><i class="fa fa-send"></i></button>
				<span id="ck_update_btn_display"></span>
			</div>
			<div class="col-sm-4">
				<fieldset>
					<legend>Sample Output from CKEditor</legend>
					<ul>
						<?php 
							$ck_id = displayckData();
							foreach ($ck_id as $value):
						?>
						<li>
							<input type="text" id="filename_title_<?php echo $value['id']; ?>" value="<?php echo $value['title']; ?>" hidden>
							<a href="#" onclick="getDatack(<?php echo $value['id'] ?>)"><i class="fa fa-file-text fa-fw"></i> <?php echo $value['title']; ?></a>
							| <button class="btn btn-danger btn-sm" title="Download as PDF file" onclick="export_pdf(<?php echo $value['id'] ?>)"><i class="fa fa-file-pdf-o fa-fw"></i> </button> | <button class="btn btn-info btn-sm" title="Download as Word file" onclick="export_word(<?php echo $value['id'] ?>)"><i class="fa fa-file-word-o fa-fw"></i> </button>
						</li>
						<?php endforeach; ?>
					</ul>
				</fieldset>
			</div>
		</div>
	</div>
  	<!-- Modal -->
	<div class="modal fade" id="loader" role="dialog" style="padding-top: 10%;">
	    <div class="modal-dialog modal-sm">
	      	<div class="modal-content">
		        <div class="modal-body">
		          	<div align="center">
						<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><br>
						<h6>Please wait while processing data..</h6>
					</div>
		        </div>
	      	</div>
	    </div>
	</div>
</body>
</html>


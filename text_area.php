<!DOCTYPE html>
<html>
	<?php 
		require('header.php'); 
		// echo date('Ymdhisa');
	?>
<body>
	<div class="container" style="margin-top: 5%;">
		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-primary">
			      	<div class="panel-heading">List of files</div>
			      	<div class="panel-body" id="text_list">
			      		<?php 
			      			$text_lists = fileList();
			      			foreach($text_lists as $text_val):
			      		?>
				      		<ul>
				      			<li hidden><input type="text" id="file_id" value="<?php echo $text_val['id']; ?>"></li>
				      			<li><a href="#" onclick="getData(<?php echo $text_val['id']; ?>)" style="text-decoration: none"><?php echo $text_val['file_name'];?></a></li>
				      		</ul>
			      		<?php endforeach;?>
			      	</div>
			    </div>
			</div>
			<div class="col-sm-8">
				<div align="center">
					<textarea id="text_area_data" style="height: 400px;" class="form-control col-sm-8" rows="5"></textarea> <br>
					<button onclick="passTextData()" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Save data"> <i class="fa fa-send fa-fw"></i></button> |
					<button onclick="clearData()" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Clear textarea"><i class="fa fa-times fa-fw"></i></button> 
					<!-- <button onclick="getData()" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Get saved data"><i class="fa fa-rotate-left fa-fw"></i></button> -->
				</div>
			</div>
		</div>
	</div>
</body>
</html>
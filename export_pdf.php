<?php

	ini_set('max_execution_time', 0);
	require_once "mpdf/mpdf.php";
	require('db.php');

	$filename = $_GET['title'];
	$id = $_GET['id'];

	$get_data = getCKDATA($id);
	if($get_data){
		$my_file = $get_data['file_name'];
		$handle = fopen($my_file, 'r');
		$data = fread($handle,filesize($my_file));
		$pdf = new mPDF();
		$pdf->WriteHTML($data);
		$pdf->Output();
	}else{
		echo 'error';
	}	

?>
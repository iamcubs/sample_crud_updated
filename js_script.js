
// function cancel(){
// 	$('#c_name').val('');
// 	$('#c_desc').val('');
// 	$('#c_course').val(0);	
// }

// function create_data(){
// 	$name = $('#c_name').val();
// 	$desc = $('#c_desc').val();
// 	$course = $('#c_course').val();

// 	if($name == '' || $desc == '' || $course == 0){
// 		alert('Please fill up all fields');
// 	}else{
// 		$.ajax({
// 			url: 'routes.php?route=create',
// 			type: 'POST',
// 			data: {'name': $name, 'desc': $desc, 'course': $course},
// 			success:function(data){
// 				// alert(data) -->PLEASE use this if you want to know what is the response from the database data in case if there is an error
// 				if(data == 'success'){
// 					cancel();
// 					alert('Successfully created a record!');
// 				}else{
// 					alert('Sorry Error');
// 				}
// 			}
// 		});
// 	}
// }

// function update_list($id){
// 	$.ajax({
// 		url: 'routes.php?route=modal_update',
// 		type: 'POST',
// 		data: {'id': $id},
// 		success:function(data){
// 			$('#modal_content').html(data);
// 			$('#myModal').modal('show');
// 		}
// 	});
// }


// function edit_user(){
// 	$edt_id = $('#edt_id').val();
// 	$edt_name = $('#edt_name').val();
// 	$edt_desc = $('#edt_desc').val();
// 	$edt_course = $('#edt_course').val();

// 	$.ajax({
// 		url: 'routes.php?route=update',
// 		type: 'POST',
// 		data: {'edt_name': $edt_name, 'edt_desc': $edt_desc, 'edt_course': $edt_course, 'edt_id': $edt_id},
// 		success:function(data){
// 			if(data == 'success'){
// 				$('#myModal').modal('hide');
// 				cancel();
// 				$('#list_id').load('index.php?page=main #list_id');
// 				$('#update_div').hide();
// 				alert($edt_name+' has been successfully updated!');
// 			}else{
// 				alert('Error');
// 			}
// 		}
// 	});

// }


// function delete_list($id){
// 	if(confirm('Are you sure you want to delete ?')){
// 		$.ajax({
// 			url: 'routes.php?route=delete',
// 			type: 'POST',
// 			data: {'id': $id},
// 			success:function(data){
// 				if(data == 'success'){
// 					$('#tr_'+$id).remove();
// 				}else{
// 					alert('Error');
// 				}
// 			}
// 		});
// 	}
// }


// function passTextData(){
// 	$text_data = $('#text_area_data').val();
// 	$.ajax({
// 		url: 'routes.php?route=create_file_text_data',
// 		type: 'POST',
// 		data: {'text_data': $text_data},
// 		success:function(data){
// 			console.log(data);
// 			$('#text_list').load('index.php?page=text_area #text_list');
// 		}
// 	});
// }

// function clearData(){
// 	$text_data = $('#text_area_data').val('');
// }


// function getData($id){
// 	// console.log()
// 	if(confirm('Are you sure you want to continue this template?')){
// 		$.ajax({
// 			url: 'routes.php?route=getTextFile_data',
// 			type: 'POST',
// 			data: {'id': $id},
// 			success:function(data){
// 				if(data != 'error'){
// 					$('#text_area_data').html(data);
// 				}
// 			}
// 		});
// 	}
// }

// function displayTextData(){

// }

// CK EDITOR START JS 

function ckEditordata(){
	$title_ck = $('#ck_title').val();
	$ck_data = CKEDITOR.instances.ckeditor.getData();
	if($ck_data == '' || $title_ck == ''){
		alert('Please fill all feilds');
	}else{
		$('#loader').modal({backdrop: 'static', keyboard: false});
		$('#loader').modal('show');
		$.ajax({
			url: 'routes.php?route=ck_editor_data',
			type: 'POST',
			data: {'ck_data': $ck_data, 'title_ck': $title_ck},
			success:function(data){
				if(data == 'success'){
					$('#loader').modal('hide');
					alert('Successfully saved!');
				}else{
					$('#loader').modal('hide');
					alert('Error');
				}
			}
		});
	}
}

function getDatack($id){
	$('#loader').modal({backdrop: 'static', keyboard: false});
	$('#loader').modal('show');
	if(confirm('Are you sure you want to update this data?')){
		$.ajax({
			url: 'routes.php?route=get_ck_data',
			type: 'POST',
			data: {'id': $id},
			success:function(data){
				getTile($id);
				CKEDITOR.instances.ckeditor.setData(data);
				// $('#ckeditor').val(data);
			}
		});
	}
	$('#loader').modal('hide');
}

function getTile($id){
	$.ajax({
		url: 'routes.php?route=getTitle',
		type: 'POST',
		data: {'id': $id},
		success:function(data){
			var split = data.split("|-|");
			// console.log(split);
			$('#loader').modal('hide');
			$('#ck_title').val(split[0]);
			$('#btn_ck_save').hide();
			$('#ck_update_btn_display').html('<button class="btn btn-warning" onclick="ck_update(\''+split[1]+'\')" id="btn_ck_update"><i class="fa fa-edit fa-fw"></i></button>');
		}
	});
}

function ck_update($id){
	$data_update_text = CKEDITOR.instances.ckeditor.getData();
	$.ajax({
		url: 'routes.php?route=update_ck_data',
		type: 'POST',
		data: {'id': $id, 'text_area_data': $data_update_text},
		success:function(data){
			alert('Successfully Updated!');
		}
	});
}

function export_pdf($id){
	$('#loader').modal({backdrop: 'static', keyboard: false});
	$('#loader').modal('show');
	var file_name = $('#filename_title_'+$id).val();
	$('#loader').modal('hide');
	// window.location.href = 'export_pdf.php?id='+$id+'&title='+file_name;
	window.open('export_pdf.php?id='+$id+'&title='+file_name, '_blank');
	// console.log('iamcubs');
	// console.log(data);
}

function export_word($id){
	$('#loader').modal({backdrop: 'static', keyboard: false});
	$('#loader').modal('show');
	var file_name = $('#filename_title_'+$id).val();
	$('#loader').modal('hide');
	window.open('export_word.php?id='+$id+'&title='+file_name, '_blank');
}
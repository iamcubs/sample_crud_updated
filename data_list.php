<!DOCTYPE html>
<html>
	<?php require('header.php'); ?>
	<body style="padding-top: 10%;">

		<div align="center">
			<div id="list_id">
				<h2>List of data</h2>
				<div style="padding-left: 12%"><a href="index.php?page=create" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle fa-fw"></i> Create New</a><br></div><br>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
						<th>Description</th>
						<th>Course</th>
						<th>Action</th>
					</thead>
					<?php
						$data_list = dataList();
						foreach ($data_list as $list){
							echo '<tr id="tr_'.$list['id'].'">
									<td>'.$list['name'].'</td>
									<td>'.$list['description'].'</td>
									<td>'.$list['course'].'</td>
									<td><button onclick="update_list('.$list['id'].')" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></button> | <button onclick="delete_list('.$list['id'].')" class="btn btn-danger btn-sm"><i class="fa fa-trash fa-fw"></i></button></td>
								  </tr>';
						}
					?>
				</table>
			</div>
		</div>
			<div class="container">
			  	<div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog modal-sm" id="modal_content">
				      	
			    	</div>
			  	</div>
			</div>

	</body>
</html>